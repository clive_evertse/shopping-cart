<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

   public function list_products()
   {

      $this->load->model('product_model');
      $id = $this->uri->segment(3, 0);


      $data['products'] = $this->product_model-> get_products($id);
      $data['main_content'] = 'view_products';
      $data['catagory_name'] = $this->product_model-> get_catagory($id);
      $this->load->view('includes/template', $data);
   }
}