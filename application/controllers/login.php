<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $data['main_content'] = 'login';
        $this->load->view('includes/template', $data);
    }

    function validate_credentials()
    {


        $this->load->library('form_validation');
        $this->form_validation->set_rules('email_address', 'Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }

        else // if the user's credentials validated...
        {
            $this->load->model('membership_model');
            $query = $this->membership_model->validate();
            if ($query)
            {
                $data = array(
                    'email' => $this->input->post('email_address'),
                    'is_logged_in' => true
                );
                $this->session->set_userdata($data);
                redirect('shop');
            }

            else
            {
                $this->index();
            }

        }

    }



    function signup()
    {
        $data['title'] = 'Signup';
        $data['main_content'] = 'register';
        $this->load->view('includes/template', $data);
    }

    function create_member()
    {


        // field name, error message, validation rules
        $this->form_validation->set_rules('first_name', 'Name', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|valid_email|unique[user.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
        $this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|required|matches[password]');


        if($this->form_validation->run() == FALSE)
        {
            $this->signup();
        }

        else
        {
            $this->load->model('membership_model');

            if($query = $this->membership_model->create_member())
            {
                $data['title'] = 'Signup Successful';
                $data['main_content'] = 'signup_successful';
                $this->load->view('includes/template', $data);
            }
            else
            {
                $data['title'] = 'Register';
                $data['title'] = 'register';
                $this->load->view('includes/template',$data);
            }
        }

    }

    public function members_area()
    {
        $this->load->view('logged_in_area');
    }

    function logout()
    {
        $array_items = array('email' => '');
        $this->session->unset_userdata($array_items);
        $this->session->sess_destroy();
        redirect('login');
    }

    function not_logged_in()
    {
        $data['main_content'] = 'login';
        $this->load->view('includes/template', $data);
    }

}