<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class billing extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->is_logged_in();
        $this->load->model('Billing_model');
    }



    public function save_order()
    {


        $order = array(
            'date' 			=> date('Y-m-d'),
            'cust_id' 	=> $this->session->userdata('id')
        );

        $ord_id = $this->Billing_model->insert_order($order);

        if ($cart = $this->cart->contents()):
            foreach ($cart as $item):
                $order_detail = array(
                    'orderid' 		=> $ord_id,
                    'productid' 	=> $item['id'],
                    'quantity' 		=> $item['qty'],
                    'price' 		=> $item['price'],
                    'time' => date('Y-m-d')
                );

                $cust_id = $this->Billing_model->insert_order_detail($order_detail);
            endforeach;
        endif;
        $this->cart->destroy();

        $data['title'] = 'Home';
        $data['main_content'] = 'order_placed';
        $this->load->view('includes/template', $data);
    }

    function is_logged_in()
    {
        $is_logged_in = $this->session->userdata('is_logged_in');
        if(!isset($is_logged_in) || $is_logged_in != true)
        {
            redirect('login/not_logged_in');
        }
    }
}