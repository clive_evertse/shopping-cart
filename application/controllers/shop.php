<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shop extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }
    public function get_member_data()
    {
        $this->load->model('membership_model');
        $query = $this->membership_model->get_member_details();;
        $this->session->set_userdata($query);
    }

    function index()
    {


        $this->load->model('product_model');
        $this->load->model('membership_model');

        $this->get_member_data();
        $data['featured_products'] = $this->product_model->get_all_featured();
        $data['bestsellers'] = $this->product_model->get_all_bestsellers();
        $data['product_names'] = $this->product_model->get_names();
        $data['products'] = $this->product_model->get_all();

        $data['title'] = 'Home';
        $data['main_content'] = 'home';
        $this->load->view('includes/template', $data);
    }

    function add()
    {
        $prod_id = $this->uri->segment(3, 0);
        $this->load->model('product_model');
        $product = $this->product_model->get($prod_id);
        $insert = array(
            'id' => $prod_id,
            'qty' => 1,
            'price' => $product->price,
            'name' => $product->name
        );
        $this->cart->insert($insert);
        redirect('shop/view_cart');
    }

    function remove()
    {
        $rowid = $this->uri->segment(3, 0);
        $qty = $this->uri->segment(4, 0) - 1;

        $this->cart->update(array(
            'rowid' => $rowid,
            'qty' => $qty
        ));

        redirect('shop/view_cart');
    }

    function update()
    {
       $rowid = $this->uri->segment(3, 0);
        $qty = $this->uri->segment(4, 0) + 1;
        $this->cart->update(array(
            'rowid' => $rowid,
            'qty' => $qty
        ));

        redirect('shop/view_cart');
    }

    function product()
    {
        $productid = $this->uri->segment(3, 0);

        $this->load->model('product_model');
        $data['product'] = $this->product_model->get($productid);


        $data['title'] = 'View Product';
        $data['main_content'] = 'view_product';
        $this->load->view('includes/template', $data);
    }

    function profile()
    {
        $this->get_member_data();
        $this->load->model('location_model', 'location');
        $data['locations'] = $this->location->get();

        $data['days'] = $this->buildDayDropdown();
        $data['months'] = $this->buildMonthDropdown();
        $data['years'] = $this->buildYearDropdown();

        $data['title'] = 'Create your profile';
        $data['main_content'] = 'create_profile';
        $this->load->view('includes/template',$data);

    }

    function buildDayDropdown($name='',$value='')
    {
        $days = '';
        while ( $days <= '31'){
            $day[]=$days;$days++;
        }
        $name = 'day';
        return form_dropdown($name, $day, $value);
    }

    function buildYearDropdown($name='',$value='')
    {
        $years = range(1900, date("Y"));
        foreach($years as $year)
        {
            $year_list[$year] = $year;
        }

        $name = 'year';

        return form_dropdown($name, $year_list, $value);
    }

    function buildMonthDropdown($name='',$value='')
    {
        $month = array(
            '01'=>'January',
            '02'=>'February',
            '03'=>'March',
            '04'=>'April',
            '05'=>'May',
            '06'=>'June',
            '07'=>'July',
            '08'=>'August',
            '09'=>'September',
            '10'=>'October',
            '11'=>'November',
            '12'=>'December');
        $name = 'month';
        return form_dropdown($name, $month, $value);
    }

    function create_profile()
    {
        $this->load->model('membership_model');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('surname', 'Surname', 'trim|required');
        $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('day', 'Day', 'trim|required');
        $this->form_validation->set_rules('month', 'Month', 'trim|required');
        $this->form_validation->set_rules('year', 'Year', 'trim|required');
        $this->form_validation->set_rules('province', 'Province', 'trim|required');
        $this->form_validation->set_rules('region', 'Region', 'trim|required');

        if($this->form_validation->run() == FALSE)
        {
            $this->profile();
        }
        else
        {
            $this->membership_model->create_profile();
        }



    }

    function subscribe()
    {
        $this->load->model('membership_model');
        $this->membership_model->subscribe();
        redirect('shop');
    }

    function view_all()
    {
        $this->load->model('product_model');

        $data['products'] = $this->product_model->get_all_cat();

        $data['title'] = 'View Products';
        $data['main_content'] = 'view_products';
        $this->load->view('includes/template',$data);
    }


    function view_cart()
    {
        $data['title'] = 'View Shopping Cart';
        $data['main_content'] = 'view_cart';
        $this->load->view('includes/template', $data);
    }

    function aboutus()
    {
        $data['title'] = 'About Us';
        $data['main_content'] = 'about_us';
        $this->load->view('includes/template', $data);
    }

    function empty_cart()
    {
        $this->cart->destroy();
        redirect('shop');
    }

}