<?php

class Location_model  extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get()
    {
        $data['provinces'] = $this->get_province();
        $data['regions'] = $this->get_regions();
        $data['suburbs'] = $this->get_suburbs();

        return $data;
    }

    function get_province()
    {
        return $this->db->get('province')->result_array();
    }

    function get_regions()
    {
        return $this->db->get('region')->result_array();
    }

    function get_suburbs()
    {
        return $this->db->get('suburb')->result_array();
    }
}