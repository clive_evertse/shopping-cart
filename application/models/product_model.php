<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 class Product_model  extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

     function get_all_featured() {

         return $this
             ->db
             ->select('product.id, product.name, product.image, product.price')
             ->from('featured_products')
             ->join('product','product.id = featured_products.product_id' , 'left')
             ->get()
             ->result_array();

     }

     function get_all_bestsellers() {

         return $this
             ->db
             ->distinct()
             ->from('best_sellers')
             ->join('product','product.id = best_sellers.product_id' , 'left')
             ->limit(9)
             ->get()
             ->result_array();

     }

     function get_all() {

         $results = $this->db->get('product')->result();

         return $results;

     }
     function get_all_cat() {

         $results = $this->db->get('product')->result_array();

         return $results;

     }

     function get($id) {

         $results = $this->db->get_where('product', array('id' => $id))->result();
         $result = $results[0];

         return $result;
     }

     function get_names()
     {
         return $this
                ->db
                ->select('*')
                ->from('product_type')
                ->get()
                ->result_array();
     }

     function get_products($id)
     {
         return $this
             ->db
             ->select('product.id, product.name, product.image, product.price')
             ->from('product')
             ->where('product_type',$id)
             ->get()
             ->result_array();
     }

     function get_catagory($id)
     {
         return $this->db->select('product_type.name')->from('product')->join('product_type','product_type.id = product.product_type' , 'left')->where('product_type',$id)->get()->row_array();
     }
}