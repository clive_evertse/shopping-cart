<?php

 class Billing_model  extends CI_Model {

     public function insert_customer($data)
     {
         $this->db->insert('user', $data);

         $id = $this->db->insert_id();

         return (isset($id)) ? $id : FALSE;
     }

     public function insert_order($data)
     {
         $this->db->insert('orders', $data);

         $id = $this->db->insert_id();

         return (isset($id)) ? $id : FALSE;
     }

     public function insert_order_detail($data)
     {
         $this->db->insert('order_detail', $data);
     }
}