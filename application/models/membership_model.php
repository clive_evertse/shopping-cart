<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 class Membership_model  extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

     function validate()
     {
         $this->db->where('email', $this->input->post('email_address'));
         $this->db->where('password', md5($this->input->post('password')));
         $query = $this->db->get('user');

         if($query->num_rows == 1)
         {
             return true;
         }

     }

     function create_member()
     {

         $new_user_insert_data = array(
             'email' => $this->input->post('email_address'),
             'password' => md5($this->input->post('password')),
             'join_date' => date('Y-m-d H:i:s', time()+7200),
             'last_activity' => date('Y-m-d H:i:s', time()+7200));

         return $this->db->insert('user',$new_user_insert_data);

     }

     function update_user_activity()
     {
         $update_member_activity = array (
             'last_activity' => date('Y-m-d H:i:s', time()+7200)
         );

         $this->db->where('id',$this->session->userdata('id'));
         $this->db->update('user',$update_member_activity);
     }

     function get_member_details()
     {
         $query = $this->db->select(' id')->from('user')->where('email',$this->session->userdata('email'))->get()->row_array();

         return $query;
     }

     function get_member_values()
     {
         $query = $this->db->select('*')->from('user')->where('email',$this->session->userdata('email'))->get()->row_array();

         return $query;
     }

     function create_profile()
     {
         $data = $this->input->post();
         print_r($data);
         die();
         $data['id'] = $this->session->userdata('id');
         $this->db->where('id',$this->session->userdata('id'));
         $this->db->insert('buyer', $data);
     }

     public function subscribe()
     {
         $data = $this->input->post();

         return $this->db->insert('subscribers',$data);
     }
}