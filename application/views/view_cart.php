
<!-- Main -->
<div id="main" xmlns="http://www.w3.org/1999/html">

    <!-- Content -->
    <div id="content">
        <!-- Featured Products -->
        <div class="products-holder" >
            <div class="top"></div>
            <div class="middle">
                <div class="label" >
                    <h3>Shopping Cart</h3>
                </div>
                <div class="cl"></div>

                <?php if ($cart = $this->cart->contents()): ?>
                    <div id="cart">
                        <?php echo form_open('billing/save_order');?>
                        <table>
                            <caption><h2><strong>Shopping Cart</strong></h2></caption>
                            </br>
                            <thead>
                            </br>
                            <tr>
                                </br>
                                <th>Item Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th> Add more</th>
                                <th>Remove One</th>
                                <th></th>
                            </tr>
                            </thead>
                            <?php foreach ($cart as $item): ?>
                                <tr id = "cartinfo">

                                    <td><?php echo $item['name']; ?></td>
                                    <?php
                                    echo form_hidden('prodid', $item['id']);?>
                                    <td>R<?php echo $item['subtotal']; ?></td>
                                    <td id = "qty" qty =<?php echo $item['qty'];?>><?php echo $item['qty'];?></td>

                                    <td class = "item"><?php echo anchor('shop/update/'.$item['rowid'].'/'.$item['qty'], 'Add More');?></td>
                                    <td class="remove">
                                        <?php echo anchor('shop/remove/'.$item['rowid'].'/'.$item['qty'],'X'); ?>
                                    </td>
                                    <td></td>
                                </tr>
                            <?php endforeach; ?>
                            <tr class="total">
                                <td colspan="2"><strong>Total</strong></td>
                                <td>R<?php echo $this->cart->total(); ?></td>
                                <td><?php echo form_submit('action', 'Checkout', 'id = "submit"'); ?></td>
                                <td><?php echo anchor('shop/empty_cart', 'Empty Cart'); ?></td>
                            </tr>
                        </table>
                    </div>
                <?php endif; ?>

                <?php echo form_close();?>

                <div class="cl"></div>
            </div>
            <div class="bottom"></div>
        </div>
        <!-- END Featured Products -->

    </div>
    <!-- END Content -->
</div>
<!-- END Main -->
</div>
</div>
<div id="footer-push"></div>
</div>
