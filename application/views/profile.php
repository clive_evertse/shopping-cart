<script type="text/javascript" language = "javascript">

    var regions = [];
    var suburbs = [];


    <?php foreach ($locations['regions'] as $region): ?>
    regions.push(["<?php echo $region['id']; ?>", "<?php echo $region['name']; ?>", "<?php echo $region['province']; ?>"]);
    <?php endforeach; ?>

    <?php foreach ($locations['suburbs'] as $suburb): ?>
    suburbs.push(["<?php echo $suburb['id']; ?>", "<?php echo $suburb['name']; ?>", "<?php echo $suburb['region']; ?>"]);
    <?php endforeach; ?>

    function getRegionOptions(province) {
        var list = [];
        var listValue = [];

        for (var j = 0; j < regions.length; j++)
        {
            if (province == regions[j][2])
            {
                list.push(regions[j][1]);
                listValue.push(regions[j][0]);
            }
        }

        document.job_form.region.options.length = 0;
        document.job_form.suburb.options.length = 0;

        for (var i = 0; i < list.length; i++) { //Repopulate region menu
            document.job_form.region.options[i] = new Option(list[i],listValue[i],0,0);
        }
    }

    function getSuburbOptions(region) {
        var list = [];
        var listValue = [];

        for (var j = 0; j < suburbs.length; j++)
        {
            if (region == suburbs[j][2])
            {
                list.push(suburbs[j][1]);
                listValue.push(suburbs[j][0]);
            }
        }

        document.job_form.suburb.options.length = 0;

        for (var i = 0; i < list.length; i++) { //Repopulate suburb menu
            document.job_form.suburb.options[i] = new Option(list[i],listValue[i],0,0);
        }
    }
</script>
<?php




$provinces = array();
$regions = array();
$suburbs = array();


foreach ($locations['provinces'] as $province)
{
    $provinces[$province['id']] = $province['name'];
}

foreach ($locations['regions'] as $region)
{
    $regions[$region['id']] = $region['name'];
}

foreach ($locations['regions'] as $suburb)
{
    $suburbs[$suburb['id']] = $suburb['name'];
}

$attributes = array('name' => 'job_form');
echo form_open('shop/create_profile',$attributes);
echo form_input('name', set_value('name'));
echo form_input('surname', set_value('surname'));
echo form_input('address', set_value('address'));
echo form_input('phone', set_value('phone'));
echo form_input('mobile', set_value('mobile'));
echo $days;
echo $months;
echo $years;



echo form_dropdown('province', $provinces, '', 'size="9" onChange="getRegionOptions(this.value);" class = "simple_form"'); ?>
<select size="9" class = "simple_form" name="region" onChange="getSuburbOptions(this.value);"></select>
<select size="9" class = "simple_form" name="suburb"></select>

<?php
echo form_submit('', 'Submit Post!');
echo form_close(); ?>