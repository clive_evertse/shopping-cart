
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
    <title><?php echo $title ;?></title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<?php echo base_url();?>css/images/favicon.ico" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/style.css" type="text/css" media="all" />


    <!--[if IE 6]>
    <link rel="stylesheet" href="<?php echo base_url();?>css/ie6.css" type="text/css" media="all" />
    <![endif]-->
    <link rel="stylesheet" href="<?php echo base_url();?>css/jquery.jscrollpane.css" type="text/css" media="all" />
    <script src="<?php echo base_url();?>js/jquery-1.7.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>js/DD_belatedPNG-min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>js/jquery.jscrollpane.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>js/jquery.jcarousel.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>js/functions.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>js/bootstrap.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>


<!-- Wrapper -->
<div id="wrapper">
    <div id="wrapper-bottom">
        <div class="shell">
            <!-- Header -->
            <div id="header">
                <!-- Logo -->
                <h1 id="logo"><a title="Home" href="#">accessories store</a></h1>
                <p class="shopping-cart"><a class="cart" href="<?php echo base_url('shop/view_cart');?>" title="Your Shopping Cart">Your Shopping Cart</a><span>Articles:</span>&nbsp;
                    <?php
                    if ($this->cart->total_items() == NULL)
                    {
                        echo '0';
                    }
                    else
                    {
                        echo $this->cart->total_items();
                    }
                    ?>
                    <span>Cost:</span>&nbsp;<?php
                    if ($this->cart->total() == NULL)
                    {
                        echo '0.00';
                    }
                    else
                    {
                        echo $this->cart->total().'.00';
                    }
                     ?>
                </p>
                <!-- Search -->
                <div class="search-expand"></div>
                <div id="search">
                    <div class="retract-button">
                        <p>retract</p>
                    </div>
                    <form action="" method="post">
                        <input type="text" class="field" value="Search" title="Search" />
                        <input type="submit" value="" class="submit-button" />
                    </form>
                </div>
                <!-- END Search -->
                <div class="cl"></div>
                <!-- Navigation -->
                <div id="navigation">
                    <ul>
                        <li><?php echo anchor('shop/', 'Home');?></li>
                        <li><?php echo anchor('shop/view_all', 'Catalog');?></li>
                        <li><?php echo anchor('shop/aboutus', 'About Us');?></li>
                    </ul>
                </div>
                <div class="cl"></div>
                <!-- END Navigation -->