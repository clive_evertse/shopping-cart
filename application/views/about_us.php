
<div id="main">

    <!-- Content -->
    <div id="content">
        <!-- Featured Products -->
        <div class="products-holder" >
            <div class="top"></div>
            <div class="label">
                <h3>About Us</h3>
            </div>

            <div class="middle">
                <div class="text-widget">
                    </br>
                    </br>
                    <p><strong>Who We Are </strong>Welcome to Office-Accessories, we are a National Distributor of office supplies, printer paper, labels, printer supplies and custom printing. Office-Supplies.US.com was founded in 2002 will have its Tenth anniversary in 2012. We are a Dun & Bradstreet rated company and will happily provide our DUNS or Federal ID number upon request.

                        Office-Accessories is one of the fastest growing distributors on the net. By hard work, customer service and great prices we have thrived during the dot com busts of many other similar stores . We are an established company both online and off and have been able to increase sales every year and for that, we thank you, our loyal customers.</p>
                </div>
                </br>
                </br>
                <div class="text-widget">
                    <p><strong>What We Do </strong>We offer over Fifty thousand items for all your office needs. Although our sales office is located on the east coast, we have made it possible, through the Internet, to shop for all of our products directly from our 75 partner warehouses strategically located throughout the U.S.. This speeds up delivery time and eliminates most overhead costs associated with the standard brick and mortar businesses.

                        These savings are passed on to you - our clients.


                        Now for the first time whether you are a small or medium size company or an individual, you can get the same competitive prices as the 'Fortune 500' companies and have it delivered next day.</p>
                </div>
                </br>
                </br>

                <div class="text-widget">
                    <p><strong>Our Mission Statement  </strong>Office-Accessories will provide each client with: a hassle free shopping experience, the highest quality products, the most competitive prices, and efficient personalized service.</p>
                </div>

            </div>
            <div class="bottom"></div>
        </div>
        <!-- END Featured Products -->
    </div>
    <!-- END Content -->
</div>
<!-- END Main -->
</div>
</div>
<div id="footer-push"></div>
</div>
