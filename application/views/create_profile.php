<script type="text/javascript" language = "javascript">

    var regions = [];
    var suburbs = [];


    <?php foreach ($locations['regions'] as $region): ?>
    regions.push(["<?php echo $region['id']; ?>", "<?php echo $region['name']; ?>", "<?php echo $region['province']; ?>"]);
    <?php endforeach; ?>

    <?php foreach ($locations['suburbs'] as $suburb): ?>
    suburbs.push(["<?php echo $suburb['id']; ?>", "<?php echo $suburb['name']; ?>", "<?php echo $suburb['region']; ?>"]);
    <?php endforeach; ?>

    function getRegionOptions(province) {
        var list = [];
        var listValue = [];

        for (var j = 0; j < regions.length; j++)
        {
            if (province == regions[j][2])
            {
                list.push(regions[j][1]);
                listValue.push(regions[j][0]);
            }
        }

        document.job_form.region.options.length = 0;
        document.job_form.suburb.options.length = 0;

        for (var i = 0; i < list.length; i++) { //Repopulate region menu
            document.job_form.region.options[i] = new Option(list[i],listValue[i],0,0);
        }
    }

    function getSuburbOptions(region) {
        var list = [];
        var listValue = [];

        for (var j = 0; j < suburbs.length; j++)
        {
            if (region == suburbs[j][2])
            {
                list.push(suburbs[j][1]);
                listValue.push(suburbs[j][0]);
            }
        }

        document.job_form.suburb.options.length = 0;

        for (var i = 0; i < list.length; i++) { //Repopulate suburb menu
            document.job_form.suburb.options[i] = new Option(list[i],listValue[i],0,0);
        }
    }
</script>
<?php
    $provinces = array();
    $regions = array();
    $suburbs = array();


    foreach ($locations['provinces'] as $province)
    {
        $provinces[$province['id']] = $province['name'];
    }

    foreach ($locations['regions'] as $region)
    {
        $regions[$region['id']] = $region['name'];
    }

    foreach ($locations['regions'] as $suburb)
    {
        $suburbs[$suburb['id']] = $suburb['name'];
    }
?>
<!-- Main -->
<div id="main" xmlns="http://www.w3.org/1999/html">

    <!-- Content -->
    <div id="content">
        <!-- Featured Products -->
        <div class="products-holder" >
            <div class="top"></div>
            <div class="middle" id="newsletter">
                <div class="label" >
                    <h3>Create Profile</h3>
                </div>
                <?php echo validation_errors(); ?>
                <div class="cl"></div>
               <?php
                    $attributes = array('name' => 'job_form');
                    echo form_open('shop/create_profile',$attributes); ?>
                <div class="field-holder"><input type="text" name = "first_name" class="field" placeholder="First Name" title="Your Name" /></div>
                <div class="field-holder"><input type="text" name = "last_name" class="field" placeholder="Last Name" title="Your Name" /></div>
                <div class="field-holder"><input type="text" name = "address" class="field" placeholder="Address" title="Your Address" /></div>
                <div class="field-holder"><input type="text" name = "mobile" class="field" placeholder="Mobile" title="Your Mobile" /></div>
                <div class="field-holder"><input type="text" name = "phone" class="field" placeholder="Phone" title="Your Phone" /></div>
                <p><span>Date of birth</span></p>
                <?php
                    echo '<p><span>Date</span></p>'.$days.'</br>';
                    echo '<p><span>Month</span></p>'.$months.'</br>';
                    echo '<p><span>Year</span></p>'.$years.'</br>';
                    echo '</br>';
                    echo form_dropdown('province', $provinces, '', 'size="1" onChange="getRegionOptions(this.value);" class = "simple_form"');
                    echo '</br>';?>
                    <select size="1" class = "simple_form" name="region" onChange="getSuburbOptions(this.value);"></select>
                    </br>
                    <select size="1" class = "simple_form" name="suburb"></select>
                    </br>


                <div class="submit-button"><input type="submit" value="Sign in" /></div>
                <?php   echo form_close(); ?>


                <div class="cl"></div>
            </div>
            <div class="bottom"></div>
        </div>
        <!-- END Featured Products -->

    </div>
    <!-- END Content -->
</div>
<!-- END Main -->
</div>
</div>
<div id="footer-push"></div>
</div>
