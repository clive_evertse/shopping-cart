
            <!-- Main -->
            <div id="main">

                <!-- Content -->
                <div id="content">
                    <!-- Featured Products -->
                    <div class="products-holder" >
                        <div class="top"></div>
                        <div class="middle">
                            <div class="label" >
                                <h3>Products</h3>
                            </div>
                            <div class="cl"></div>

                            <?php foreach($products as $product):?>

                            <div class="product" >

                                <?php echo form_open('shop/add');
                                echo '<img  src ='.base_url(). 'images/' . $product['image'].' class = thumb alt = "'.  $product['name'].'"/>'; ?>
                                <div class="desc">
                                    <p class="name"><?php echo $product['name']; ?></p>

                                </div>
                                <div class="price-box">
                                    <p>R <span class="price"><?php echo $product['price']; ?></span></p>

                                    <?php echo form_hidden('id', $product['id']); ?>
                                </div>

                                <div class="cl"></div>
                                <?php
                                $data = array(
                                    'prod_id' => $product['id'],
                                    'id' => 'addItem',
                                    'name' => 'action',
                                    'value' => 'Add to Cart');

                                echo anchor('shop/add/'.$product['id'], 'Add to Cart') ;


                               echo form_close();?>
                                </div>
                            <?php   endforeach;?>

                            <div class="cl"></div>
                        </div>
                        <div class="bottom"></div>
                    </div>
                    <!-- END Featured Products -->

                </div>
                <!-- END Content -->
            </div>
            <!-- END Main -->
        </div>
    </div>
    <div id="footer-push"></div>
</div>
