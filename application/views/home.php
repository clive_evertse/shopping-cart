
    <!-- Dropdown Navigation -->
    <div id="sort-nav">
        <span class="label-left"></span>
        <div class="label-bg">
            <ul>
                <li>
                    <a title="Collections" href="#">Collections</a>
                    <div class="dd">
                        <ul>
                            <?php foreach($product_names as $names):?>
                                 <li><?php echo anchor('product/list_products/'.$names['id'],$names['name']);?></li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </li>
                <li>

                </li>
                <li class="last">

                </li>
            </ul>
        </div>
        <span class="label-right"></span>
    </div>
    <!-- END Dropdown Navigation -->
    <div class="cl"></div>
</div>
<!-- END Header -->
<!-- Main -->
<div id="main">
<!-- Slider -->
<div class="slider-holder">
    <div class="promo">
        <img src="<?php echo base_url();?>css/images/promo.jpg" alt="Big Sale This Week -30% OFF!" />
    </div>
    <div id="slider">
        <ul>
            <li>
                <img src="<?php echo base_url();?>css/images/slide1.jpg" alt="colour pencils" title="" />
                <div class="caption">
                    <h2>December Specials</h2>
                    <div class="text"><p>LGet the best office supply's</p></div>
                    <div class="buy-now">
                        <p>BUY NOW</p>
                        <p class="price">R<span>0.99</span></p>
                    </div>
                    <div class="cl"></div>
                    <p><strike>R1.29</strike></p>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url();?>css/images/slide2.jpg" alt="pencils"  title=""/>
                <div class="caption">
                    <h2>Keep your cash in a safe place</h2>
                    <div class="text">
                        <p>Buy office equipment from us</p></div>
                    <div class="buy-now">
                        <p>BUY NOW</p>
                        <p class="price">R<span>0.99</span></p>
                    </div>
                    <div class="cl"></div>
                    <p><strike>R1.29</strike></p>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url();?>css/images/slide1.jpg" alt="pencils" />
                <div class="caption">
                    <h2>Need post its</h2>
                    <div class="text">
                        <p>Buy office equipment from us</p>
                    </div>
                    <div class="buy-now">
                        <p>BUY NOW</p>
                        <p class="price">R<span>0.99</span></p>
                    </div>
                    <div class="cl"></div>
                    <p><strike>R1.29</strike></p>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url();?>css/images/slide2.jpg" alt="pencils" />
                <div class="caption">
                    <h2>Some  Goes Here</h2>
                    <div class="text">
                        <p>Lorem ipsum dolo adipiscing elit. Ut tempus malesuada lobortis. Sed id lobortis est.</p>
                    </div>
                    <div class="buy-now">
                        <p>BUY NOW</p>
                        <p class="price">R<span>0.99</span></p>
                    </div>
                    <div class="cl"></div>
                    <p><strike>R1.29</strike></p>
                </div>
            </li>
            <li>
                <img src="css/images/slide1.jpg" alt="pencils" />
                <div class="caption">
                    <h2>Some Tittle Goes Here</h2>
                    <div class="text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tempus malesuada lobortis. Sed id lobortis est.</p>
                    </div>
                    <div class="buy-now">
                        <p>BUY NOW</p>
                        <p class="price">R<span>0.99</span></p>
                    </div>
                    <div class="cl"></div>
                    <p><strike>R1.29</strike></p>
                </div>
            </li>
        </ul>
    </div>
    <div class="cl"></div>
    <div class="jcarousel-control">
        <a title="Slide 1" href="#">1</a>
        <a title="Slide 2" href="#">2</a>
        <a title="Slide 3" href="#">3</a>
        <a title="Slide 4" href="#">4</a>
        <a title="Slide 5" href="#">5</a>
    </div>
</div>
<!-- END Slider -->
<!-- Content -->
<div id="content">
    <!-- Featured Products -->
    <div class="products-holder">
        <div class="top"></div>
        <div class="middle">
            <div class="label">
                <h3>Feature Products</h3>
            </div>
            <div class="cl"></div>
            <?php foreach ($featured_products as $featured_product): ?>
            <div class="product" >
                <div class="desc">
                    <p class="name"><?php echo $featured_product['name']; ?></p>
                    </div>
                    <div class="thumb">
                       <?php $data = array(
                            'title' => $featured_product['name']

                        );
                        echo anchor('shop/product/'.$featured_product['id'],'<img  src ='. base_url().'images/' . $featured_product['image'].' class = thumb alt = "'.  $featured_product['name'].'"/>', $data);?>
                    </div>
                <div class="price-box">
                    <p>R <span class="price"><?php echo $featured_product['price']; ?></span></p>

                    <?php echo form_hidden('id', $featured_product['id']); ?>
                </div>


                    <?php echo form_hidden('id', $featured_product['id']);
                    $data = array(
                        'prod_id' => $featured_product['id'],
                        'id' => 'submitItem',
                        'name' => 'action',
                        'value' => 'Add to Cart');?>

                </li>
             </div>
            <?php endforeach; ?>
                <div class="cl"> </div>


            </div>


            <div class="cl"></div>
        </div>
        <div class="bottom"></div>
    </div>
    <!-- END Featured Products -->
    <!-- Best Sellers -->
    <div class="products-holder">
        <div class="top"></div>
        <div class="best-sellers middle">
            <div class="label">
                <h3>Best Sellers</h3>
            </div>
            <div class="cl"></div>
            <!-- Scroll Pane -->
            <div class="left-border"></div>
            <div class="scroll-pane horizontal-only">
                <?php foreach ($bestsellers as $bestseller):?>
                <div class="product">
                    <?php
                    $data = array(
                        'title' => $bestseller['name']

                    );
                    echo anchor('shop/product/'.$bestseller['id'],'<img  src ='. base_url().'images/' . $bestseller['image'].' class = thumb alt = "'.  $bestseller['name'].'"/>', $data);?>
                    <div class="desc">
                        <p class="name"><?php echo $bestseller['name']; ?></p>


                    </div>
                    <div class="price-box">
                        <p>R <span class="price"><?php echo $bestseller['price']; ?></span></p>
                        <p class="per-peace">Per Peace</p>
                    </div>

                </div>
            <?php endforeach; ?>
            </div>
            <!-- END Scroll Pane -->
            <div class="right-border"></div>
            <div class="cl"></div>
        </div>
        <div class="bottom"></div>
    </div>
    <!-- END Best Sellers -->
    <!-- Bottom Strip -->
    <div class="bottom-strip">
        <div class="box-holder left">
            <div class="box">
                <div class="top"></div>
                <div class="label">
                    <h3>About Us</h3>
                </div>
                <div class="middle">
                    <div class="text-widget">
                        <p><strong>Mauris euismod lorem </strong>at mauris gravida tincidunt. Maecenas purus arcu, luctus at suscipit tempor, pharetra et urna. Quisque molestie purus sit amet ligula varius a adipiscing ligula molestie. Morbi fermentum lobortis turpis mattis bibendum. In hac habitasse platea </p>
                    </div>
                    <a class="read-more" title="Read More" href="#">Read More</a>
                </div>
                <div class="bottom"></div>
            </div>
        </div>
        <div class="box-holder middle">
            <div class="box">
                <div class="top"></div>
                <div class="label">
                    <h3>Newsletter</h3>
                </div>
                <div class="middle" id="newsletter">
                    <div class="post">
                        <p><span>Subscribe to our newsletter</span></p>
                        <p>Praesent et ultrices turpis. Donec at est vel neque dictum aliquet.</p>
                    </div>
                    <div class="cl"></div>
                    <?php echo form_open('shop/subscribe'); ?>
                        <div class="field-holder"><input type="text" name = "email" class="field" value="Your Email" title="Your Email" /></div>
                        <div class="field-holder"><input type="text" name = "name" class="field" value="Your Name" title="Your Name" /></div>
                        <div class="submit-button"><input type="submit" value="Subscribe" /></div>
                    </form>
                    <div class="cl">&nbsp;</div>
                </div>
                <div class="bottom"></div>
            </div>
        </div>
        <div class="box-holder right">
            <div class="box">
                <div class="top"></div>
                <div class="label">
                    <h3>Latest Posts</h3>
                </div>
                <div class="middle post-short">
                    <div class="post">
                        <p><span>Aenean placerat</span></p>
                        <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae tellus t turpisporttitor pulvinar ...</p>
                        <a class="comments" href="#" title="comments">comments</a>
                    </div>
                    <div class="date-box">
                        <p class="date">05.26</p>
                        <p>2011</p>
                    </div>
                    <div class="cl"></div>
                </div>
                <div class="bottom"></div>
            </div>
        </div>
        <div class="cl"></div>
    </div>
    <!-- END Bottom Strip -->
</div>
<!-- END Content -->
</div>
<!-- END Main -->
</div>
</div>
